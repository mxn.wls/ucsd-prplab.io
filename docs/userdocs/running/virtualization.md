Guide: <https://kubevirt.io/2020/KubeVirt-installing_Microsoft_Windows_from_an_iso.html>

###### Creating the new VM from existing image

A new PersistentVolume can be filled with an image of a VM with help of <https://github.com/kubevirt/containerized-data-importer>:

```yaml
apiVersion: cdi.kubevirt.io/v1beta1
kind: DataVolume
metadata:
  name: "windev"
spec:
  source:
      http:
         url: "https://yumrepo.nrp-nautilus.io/vm/Win10_20H2_v2_English_x64.iso"
  pvc:
    volumeMode: Block
    accessModes:
      - ReadWriteOnce
    resources:
      requests:
        storage: "10Gi"
```

This will create a new PVC and pull the data in it. Once it's ready:

```
➜ kubectl get datavolume windev
NAME   PHASE       PROGRESS   RESTARTS   AGE
windev Succeeded   100.0%                3d19h
```

... you can create a drive for the VM:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: winhd
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 50Gi
  storageClassName: linstor-nrp-unl
```

... and a VM with both:

```yaml
apiVersion: kubevirt.io/v1
kind: VirtualMachine
metadata:
  name: winvm
spec:
  running: false
  template:
    metadata:
      labels:
        kubevirt.io/size: small
        kubevirt.io/domain: winvm
    spec:
      domain:
        cpu:
          cores: 4
        devices:
          disks:
            - bootOrder: 2
              cdrom:
                bus: sata
              name: cdromiso
            - disk:
                bus: virtio
              bootOrder: 1
              name: harddrive
            - cdrom:
                bus: sata
              name: virtiocontainerdisk
          interfaces:
          - name: default
            masquerade: {}
          inputs:
          - bus: usb
            name: tablet1
            type: tablet
        resources:
          requests:
            memory: 10Gi
          limits:
            memory: 10Gi
      networks:
      - name: default
        pod: {}
      volumes:
        - name: cdromiso
          dataVolume:
            name: windev
        - name: harddrive
          persistentVolumeClaim:
            claimName: winhd
        - containerDisk:
            image: kubevirt/virtio-container-disk
          name: virtiocontainerdisk
```

Install virtctl:
<https://github.com/kubevirt/kubevirt/releases/tag/v0.42.1>

And start it:

`virtctl start winvm`

After that you should be able to connect to it using vnc: `virtctl vnc winvm` after instaling one of VNC apps on mac:

<https://github.com/kubevirt/kubevirt/blob/master/pkg/virtctl/vnc/vnc.go#L223>

<https://kubevirt.io/user-guide/#/usage/graphical-and-console-access>

#### Our main example

```yaml
apiVersion: kubevirt.io/v1
kind: VirtualMachine
metadata:
  name: winvm
  namespace: dimm
spec:
  running: false
  template:
    metadata:
      labels:
        kubevirt.io/size: small
        kubevirt.io/domain: winvm
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: kubernetes.io/hostname
                operator: In
                values:
                - hcc-nrp-shor-c5830.unl.edu
      domain:
        cpu:
          cores: 4
        devices:
          gpus:
          - deviceName: "nvidia.com/A40_Tesla_A40"
            name: gpu1
          disks:
            - bootOrder: 2
              cdrom:
                bus: sata
              name: cdromiso
            - disk:
                bus: virtio
              bootOrder: 1
              name: harddrive
            - cdrom:
                bus: sata
              name: virtiocontainerdisk
          interfaces:
          - name: default
            masquerade: {}
          inputs:
          - bus: usb
            name: tablet1
            type: tablet
        resources:
          requests:
            memory: 10Gi
          limits:
            memory: 10Gi
      networks:
      - name: default
        pod: {}
      volumes:
        - name: cdromiso
          dataVolume:
            name: windev     
        - name: harddrive
          persistentVolumeClaim:
            claimName: winhd
        - containerDisk:
            image: kubevirt/virtio-container-disk
          name: virtiocontainerdisk
```

#### Adding virtvnc

In your namespace create a deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: virtvnc
spec:
  replicas: 1
  revisionHistoryLimit: 1
  selector:
    matchLabels:
      app: virtvnc
  template:
    metadata:
      labels:
        app: virtvnc
    spec:
      containers:
      - env:
        - name: NAMESPACE
          valueFrom:
            fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
        image: gitlab-registry.nrp-nautilus.io/prp/virtvnc
        imagePullPolicy: Always
        livenessProbe:
          failureThreshold: 30
          httpGet:
            path: /
            port: 8001
            scheme: HTTP
          initialDelaySeconds: 30
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 5
        name: virtvnc
        resources:
          requests:
            cpu: 1m
            memory: 100Mi
          limits:
            cpu: 2m
            memory: 200Mi
      serviceAccountName: virtvnc
```

Service account and roles:

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: virtvnc
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: virtvnc
rules:
- apiGroups:
  - subresources.kubevirt.io
  resources:
  - virtualmachineinstances/console
  - virtualmachineinstances/vnc
  verbs:
  - get
- apiGroups:
  - kubevirt.io
  resources:
  - virtualmachines
  - virtualmachineinstances
  - virtualmachineinstancepresets
  - virtualmachineinstancereplicasets
  - virtualmachineinstancemigrations
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: virtvnc
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: virtvnc
subjects:
- kind: ServiceAccount
  name: virtvnc
```

Service and ingress:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    ingress.kubernetes.io/auth-secret: virtvnc-login
    ingress.kubernetes.io/auth-type: basic
    ingress.kubernetes.io/config-backend: |
      http-request del-header Authorization    
    kubernetes.io/ingress.class: haproxy
  name: virtvnc
spec:
  rules:
  - host: <some_name>.nrp-nautilus.io
    http:
      paths:
      - backend:
          service:
            name: virtvnc
            port:
              number: 8001
        path: /
        pathType: ImplementationSpecific
  tls:
  - hosts:
    - <some_name>.nrp-nautilus.io
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: virtvnc
  name: virtvnc
spec:
  ports:
  - port: 8001
    protocol: TCP
    targetPort: 8001
  selector:
    app: virtvnc
  type: ClusterIP
```

Secret to hold the password for your virtvnc:

`kubectl create secret generic virtvnc-login -n <namespace> --from-literal=auth=<my_login>::<my_password>`
